import config
import shutil
import uvicorn
from deepface import DeepFace

from utils import process_image
from fastapi import FastAPI, UploadFile, File, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from starlette.responses import HTMLResponse

app = FastAPI(
    title=config.PROJECT_TITLE,
    description=config.PROJECT_DESC,
    version=config.VERSION
    )

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="template")

model = DeepFace.build_model("Facenet")

@app.get('/', response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse("index.html", {"request":request})

@app.post('/verify', response_class=HTMLResponse)
async def verify_id(request:Request, img1: UploadFile = File(...), img2: UploadFile = File(...)):
    """
    Verify the faces in the given two images

    Args:
    img1: Binary Image File Input from FORM
    img2: Binary Image File Input from FORM

    Return:
    result : Boolean
    """
    img1 = process_image(img1)
    img2 = process_image(img2)
    result = DeepFace.verify(img1, img2, model=model)
    print(result)
    result = result["verified"]
    return templates.TemplateResponse("result.html", {"request":request, "result":result})

if __name__ == "__main__":
    uvicorn.run("main:app", port=8888, reload=True)