<p align="center">
<img src="src/static/logo.png" width=100 />
</p>

# FaceID - Identity Verification Application
FaceID is a Deep Learning based facial recognition system to verify the users face with his/her respective id card. It comes with user friendly web application is used to get images from the user and process in backend server that authenticate the user to move further. FaceID is written in modular fashion that makes it easy to integrate with other applications.

![Demo](demo.gif)

## Architecture 🏛️
![architecture](/designs/architecture.png)
Both the images are fed into to the model to detect the face and generate encodings. Then, using a similarity fucntion each generated encodings is compared to verify the face. 

## Tech Stack 💻
- Python3
- DeepFace API
- FastAPI
- Jinja2
- CSS3
- HTML5
- JavaScript

## Prerequisites 📜
- Python 3.7+
- Git

## Instructions 📘
1. Clone the repo using `git clone https://gitlab.com/Ferozkhanroz/faceid.git`
2. Open your terminal in the repo cloned on your pc.
3. Run `pip install -r requirements.txt` to install dependencies.
4. Execute these commands to run the app.
```
>>> cd src/
>>> python main.py
```
5. You can find the application running on [localhost:8888](localhost:8888)

## References 🌎
A lot of resources in the internet helped me to learn these things and create this app. I owe them an acknowledgment.
- FaceRecognition API - [DeepFace](https://pypi.org/project/deepface/) documentation 
- Application developed using [FastAPI Backend Server](https://fastapi.tiangolo.com) and [Jinja2 Templating Service](https://jinja.palletsprojects.com/en/2.11.x/)
- Face recognition framework wrapping state-of-the-art models: [VGG-Face](https://sefiks.com/2018/08/06/deep-face-recognition-with-keras/), Google [FaceNet](https://sefiks.com/2018/09/03/face-recognition-with-facenet-in-keras/), [OpenFace](https://sefiks.com/2019/07/21/face-recognition-with-openface-in-keras/), Facebook [DeepFace](https://sefiks.com/2020/02/17/face-recognition-with-facebook-deepface-in-keras/), [DeepID](https://sefiks.com/2020/06/16/face-recognition-with-deepid-in-keras/), [ArcFace](https://sefiks.com/2020/12/14/deep-face-recognition-with-arcface-in-keras-and-python/) and [Dlib](https://sefiks.com/2020/07/11/face-recognition-with-dlib-in-python/).